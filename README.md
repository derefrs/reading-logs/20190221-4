# 20190221 - Deref.rs \#4

Thu, 21. Feb. 2019

https://derefrs.connpass.com/event/12064/

```toml
[meetup]
name = "Deref Rust"
date = "20190221"
version = "0.0.4"
attendees = [
  "Akihiro Iwata <akihiro.iwata.1990@gmail.com>",
  "Yasuhiro Яша Asaka <yasuhiro.asaka@grauwoelfchen.net>",
]
repository = "https://gitlab.com/derefrs/reading-logs/20190221-4"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @akihiro-iwata | [Why should you use Rust in WebAssembly?](https://qiita.com/akihiro-iwata/items/f8ea6272a919665402db) |
| @grauwoelfchen | [Read Chapter 19. "Unsafe Rust"](https://nostarch.com/Rust) (978-1-59327-828-1) |


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2019 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
