# Deref\\.rs #4

https://derefrs.connpass.com/

//! <del>rustup</del> rust meetup.  
//! A meetup to deref each other about reading Rust ;)

```text
ᕕ( ᐛ )ᕗ
----------- value moved here
```

[日本語バージョン](
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/NOTICE.ja.md) も
あります.


## Overview

Deref\\.rs ー Deref Rust

#### Date

Thu Feb 21 19:20:00 JST 2019 (- 20:55)

#### What we do

Read an article, book or paper alone/together about Rust for fun, and share
just what you learn. That't it. You are very welcome also to make something.
Please show off what you make ;)


```rust
#[derive(Fun)]
```

#### Location

```toml
[place]
version = "0.1.0"
edition = "2018"

[[PARK6]]
address = "Roppongi 6-10-1 (Roppongi Hills West Walk 6F)"
website = "https://www.park6.jp/"
```

If attendees are more than 4 before 2 days the day, organizer is going to
try to book a meeting room. But normally, we have the meetup in general cafe
space.

We don't have any restriction for this place, but please order somehting
to drink/eat because it's normal cafe ;)


## Learing Rust

Just read something what you want to, e.g. articles posted on feeds about Rust.


#### References

```zsh
% cargo build
    Compiling our references v.0.0.1 (https://derefrs.connpass.com/)
error: cannot read all references if you are alone!
```

Books and feeds:
https://gitlab.com/derefrs/references/blob/master/README.rst

```text
- first borrow ends here
```

## Contents

#### Topic

Just focus on something about Rust for fun. (We talk in English or Japanese)

#### Timetable

| Time | We do |
|--|--|
| 19:20 | Gather around |
| 19:30 | Self-introduction |
| 19:35 | Reading (coding) Time |
| 20:20 | Pause |
| 20:25 | Presentation (if you want) |
| 20:55 | Finishing up |

We'll have at least 45 minutes for the reading (writing). But talking while the
time also very welcome. Please feel free asking each other.
In another half (30 min.), share what you learn/read.

You can do anything about Rust, but please make sure:

* Tell others what you do (on your topic in Zulip chat)
* Deref (share) what you learned after that
* Post a note (It's optional, a snippets on GitLab.com's or your blog etc.)

#### Tools

You'll be invited for a group on Zulip and GitLab\\.com.

##### Chat

On `reading-logs` channel (in Zulip), we'll have **topics** by attendees.
Feel free to post anything on your/other's **topic** while the meetup.

https://derefrs.zulipchat.com/

##### Reading Logs

A project on GitLab\\.com (under the [Reading Logs](
https://gitlab.com/derefrs/reading-logs) Group), will be made for every meetup.
If you want to make a note on there, you can use **sinnpets**.

https://gitlab.com/derefrs/reading-logs/

## Policies

* Please tell your delay or accidentaly cancellation to an organizer
* Any sales promotion is not allowed (please don't)
* We'll take an action if you do something bad, especially for other users

#### Code of Conduct

We do this meetup along Rust's [Code of Conduct](
https://www.rust-lang.org/en-US/conduct.html) (as a reference).
And we hope that all enjoy this meetup, but if you have a problem
or find something kind of violation, please share it to the organizer
([Yasuhiro Яша Asaka](https://twitter.com/grauwoelfchen)) at anytime.


## Links

* [NOTICE.md](
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/NOTICE.md)  
  This information
* [Reading Logs](
https://gitlab.com/derefrs/reading-logs/20190221-4)  
  Our notes

## License

This notice is published under the MIT License and it's a document for
Deref\\.rs.

Reading logs and notes by attendees, are belong to them.

```text
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/LICENSE
Copyright (c) 2019 Deref.rs
```

```text
 ᕕ( ᐛ )ᕗ
^^^^^ value used here after move
```
