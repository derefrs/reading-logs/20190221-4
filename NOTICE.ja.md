# Deref\\.rs #4

https://derefrs.connpass.com/

//! <del>rustup</del> rust meetup.   
//! Rust についての読書・取り組みをお互いに Dereference し合う会 ;)

```text
ᕕ( ᐛ )ᕗ
----------- value moved here
```

There is also another [version](
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/NOTICE.md) in
English.


## 概要

Deref\\.rs ー Deref Rust

#### 日時

2019 年 02 月 21 日 (木) 19:20 - 20:55

#### 内容

興味あるものをそれぞれ持ちよって, それぞれ各自で読み, 共有し合うことで楽しんで
Rust を勉強していきます.
基本的に何か (Blog 記事, 書籍, 論文 etc.) を読む会ですが, そこから Inspiration
を受けて何かを作ったり試したりすることも歓迎します.


```rust
#[derive(Fun)]
```

#### 場所

```toml
[開催場所]
version = "0.1.0"
edition = "2018"

[[PARK6]]
address = "東京都港区六本木6-10-1 六本木ヒルズ森タワー ウェストウォーク6F"
website = "https://www.park6.jp/"
```

2 日以上前に 4 人以上集ることがわかった場合は, 同カフェスペース内の会議室など
個室の予約を試みます.  
通常は一般のカフェスペースで開催します.

とくに制約はありませんが一般のお店なのでできたら何か注文してください ;)

## Learing Rust

各自好きに持ちよった書籍や Rust についてのキュレーション Feed で紹介されている
記事などをのんびり読んでいきます.  
\# もし希望があればみんなで同じものを一緒に読むようなこともあるかもしれません.

#### リファレンス

```zsh
% cargo build
    Compiling our references v.0.0.1 (https://derefrs.connpass.com/)
error: cannot read all references if you are alone!
```

書籍や Feed の参考リスト:  
https://gitlab.com/derefrs/references/blob/master/README.rst

```text
- first borrow ends here
```

## 内容

#### テーマ

Rust に関することを主な内容としてください. (英語または日本語で話します)

#### 進め方

| 時間 | 内容 |
|--|--|
| 19:20 | カフェ 集合 |
| 19:30 | 自己紹介 |
| 19:35 | 読書またはコーディング開始  |
| 20:20 | 休憩 |
| 20:25 | 発表タイム |
| 20:55 | 終了 |

45 分を集中してそれぞれ読む (あるいはコードを書いたりする) 時間とします.  
おしゃべりも歓迎します. 残りの 30 分間で, 順番に各自, 読んだ内容や
やったこと・作ったものを簡単に発表しましょう.

内容は Rust に関することで楽しいことであれば何でも構いませんが

* 始める前にみんなに何をするかを共有する (Zulip Chat の各自の Topic)
* 終ったら成果や理解した内容を簡単に発表する
* 書いてまとめる (Optional. GitLab\\.com の Snippets またはご自身の Blog etc.)

という形で進行します.

#### Tools

Zulip chat と GitLab\\.com に招待します.

##### Chat

Zulip にある `reading-logs` channel に, 各参加者ごとの **topic** を作ります.
メモや気になったことなど, 自分の **topic** に投稿しながら読み進めてください.

https://derefrs.zulipchat.com/

##### Reading Logs

GitLab\\.com にある [Reading Logs](
https://gitlab.com/derefrs/reading-logs) Group 下 に開催日毎に Project を
作ります. 各自好きなフォーマットで何か書いて, 掲載用の URL をお知らせください.
希望される方は同 Project の **snippets** に投稿できるようにします.

https://gitlab.com/derefrs/reading-logs/

## ポリシー

* 遅刻や急遽キャンセルされる場合はご連絡ください.
* 営業行為はご遠慮ください.
* 他の方が不快に感じるような行為, イベントの趣旨・目的から逸脱した行為を取られた
  方は, 退出および次回以降の参加をお断りさせて頂くことがあります.

#### Code of Conduct

Rust の [Code of Conduct](
https://www.rust-lang.org/en-US/conduct.html) に準ずる方針で活動します.
何もなく楽しく進められることを願いますが, 万一, 懸念点や問題があれば,
Organizer ([Yasuhiro Яша Asaka](
https://twitter.com/grauwoelfchen)) までご連絡ください.


## リンク

* [NOTICE.ja.md](
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/NOTICE.ja.md)  
  開催案内
* [Reading Logs](
https://gitlab.com/derefrs/reading-logs/20190221-4)  
  読書ノート

## ライセンス

この告知文書は MIT ライセンスで公開されている Deref\\.rs 開催資料の一部です.  
参加者の読書ログ・ノートは作成者本人に帰属します.

```text
https://gitlab.com/derefrs/reading-logs/20190221-4/blob/master/LICENSE
Copyright (c) 2019 Deref.rs
```

```text
 ᕕ( ᐛ )ᕗ
^^^^^ value used here after move
```
